## Welcher Server? 
Live oder Test Server
 
## Wann?
Datum und Uhrzeit
 
## Zusammenfassung des Problems
Was ist genau passiert?
 
## Reproduzierbar? 
Ist es möglich den Fehler zu reproduzieren?
 
## Wenn ja wie?
1. Schritt 1
2. Schritt 2
3. Schritt 3 usw usw
 
## Spielerinformationen
Euren Ingame Charakter Namen so wie eure SteamID64 (https://steamid.io/)
 
## Screenshots, Logs oder Videos
Hier könnt ihr relevante Screenshots, Logs oder Videos anhängen bzw. Verlinken die das Problem erklären, zeigen oder uns ermöglichen den Fehler zu identifizieren
 
## Zusätzliche Informationen
Was euch sonst noch zu dem Fehler einfällt



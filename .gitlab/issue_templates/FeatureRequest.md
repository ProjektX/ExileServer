## **Beschreibung des Vorschlags**
  
Mod, Missions oder eine komplett andere Idee?


## **Kurze Zusammenfassung**
 
Erklärt hier in kurzen Worten euer Vorschlag oder Idee.
 

## **Links/Quelle**
 
Sollte es Downloadlinks oder eine Quelle zu der Idee geben hier hinzufügen.
 

## **Screenshots, Logs oder Videos**
 
Hier könnt ihr relevante Screenshots, Logs oder Videos anhängen bzw. Verlinken.
 

## **Zusätzliche Informationen**
 
Hier könnt ihr ausführlichere Informationen zu eurer Idee hinterlegen.


